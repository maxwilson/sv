import { BrowserRouter, Route, Redirect } from 'react-router-dom';

import { isAuthenticated } from './services/auth';
import Person from './pages/persons/Person';
import Login from './pages/login/Login';

function RoutePrivatePerson() {

  if(!isAuthenticated()) {
    return <Redirect to={{ pathname: "/" }} />
  }else {
    return <Route path="/person" component={Person} />;
  }
}

function Routes() {
  return(
    
    <BrowserRouter>
      <Route path="/" exact component={Login} />
      <RoutePrivatePerson />
    </BrowserRouter>

  );
}

export default Routes;