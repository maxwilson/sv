import React,{ FormEvent, useState } from 'react';

import './styles.css';
import 'bulma/css/bulma.css';
import api from './../../services/api';
import {login} from '../../services/auth';
import { useHistory } from 'react-router-dom';

function Login() {
  const history = useHistory();
  const [ email, setEmail ] = useState('');
  const [ password, setPassword ] = useState('');

  async function handleLogin(e: FormEvent) {
    e.preventDefault();
  

    await api.post('auth/login', {
      email,
      password,
    }).then(
      function (response) {
        login(response.data.access_token);
        history.push('/person');
      }
    ).catch(() => {
      alert('Senha ou Email invalido');
    });
  }

  return (
    <div className="section is-fullheight">
      <div className="container">
        <div className="column is-4 is-offset-4">
          <div className="box">
            <form onSubmit={handleLogin}>
              <div className="field">
                <label className="label">Email</label>
                <div className="control">
                  <input 
                    className="input" 
                    type="email" 
                    name="email"
                    value={email} 
                    onChange={(e) => {setEmail(e.target.value)}}/>
                </div>
              </div>
              <div className="field">
                <label className="label">Senha</label>
                <div className="control">
                  <input 
                    className="input" 
                    type="password" 
                    name="password" 
                    value={password} 
                    onChange={(e) => {setPassword(e.target.value)}}
                    required />
                </div>
              </div>
              <button type="submit" className="button is-block is-info is-fullwidth">Login</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;