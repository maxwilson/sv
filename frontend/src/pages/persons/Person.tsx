import React,{ useState, useEffect } from 'react';
import PersonItem, { IsPerson } from '../../components/personItem';

import '../../components/personItem/styles.css';
import api from './../../services/api';

function Person(){
  const [persons, setPersons] = useState([]);

  useEffect(() => {
    async function getPerons() {
      const response = await api.get('/person');
    
      setPersons(response.data);
    }
    getPerons();
  },[]);

  return (
    <div className='wrapper'>
      <div className='card frame'>
        {persons.map((persons: IsPerson) => {
          return <PersonItem key={persons.id} person={persons} />
        })}
      </div>
    </div>
  )
}

export default Person;