import  React from 'react';
import './styles.css';

export interface IsPerson {
  "id": number;
  "nome": string;
  "email"?: string;
  "sexo"?: string;
  "data_nascimento": string;
  "naturalidade": string;
  "nacionalidade": string;
  "cpf": string;
  "created_at": string;
  "updated_at": string; 
}

interface PersonItemProps {
  person: IsPerson;
}

const PersonItem:React.FC<PersonItemProps> = ({ person }) => {
  return (
    <div className='card-header' >
      
      <h1 className='card-header-title header center'>
        Nome: {person.nome}
        Email: {person.email}<br/>
        Sexo: {person.sexo}<br/>
        Data de Nascimento : {person.data_nascimento}<br/>
        Naturalidade: {person.naturalidade}<br/>
        Nacionalidade: {person.nacionalidade}<br/>
        CPF: {person.cpf}<br/>
        Criado em: {person.created_at}<br/>
        Atualizado em: {person.updated_at}<br/>
      </h1>
    </div>
  );
}

export default PersonItem;