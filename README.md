# SV Data Image

Desafio de trabalho

# 1 - Git Clone no projeto

No console der **[ git clone https://gitlab.com/maxwilson/sv.git ]**

# 2 - Acessando diretorio do backend

- Configure o arquivo .env do laravel de acordo com seu banco mysql.
- Inicie Migrations **[ php artisan migrate:fresh ]**

- Inicie usuário start da aplicação **[ php artisan db:seed ]**. Feito possui usuario para efetua login.

- email:dev@sv.com.br 
- password:sv@123

- No repositorio tem workpace do postman para importação.

- Rotas da aplicação
  
  **[ POST http://127.0.0.1:8000/api/auth/login ]** Efetuar autenticação;

  **[ POST http://127.0.0.1:8000/api/auth/logout ]** deslogar da aplicação;

  **[ GET http://127.0.0.1:8000/api/person ]** Recupera usuarios cadastro no sistema

  **[ POST http://127.0.0.1:8000/api/person ]** Cria usuario no banco

  **[ GET http://127.0.0.1:8000/api/person/1 ]** Recupera usuario por ID

  **[ DELETE http://127.0.0.1:8000/api/person/1 ]** Delete um usuario

  **[ PUT http://127.0.0.1:8000/api/person/4 ]** Atualiza usuario no banco

# 3 - Efetuando login

 **[ POST http://127.0.0.1:8000/api/auth/login ]** essa routa retorna access_token.
 Caso utiliza o postman adicionei o token Authorization -> Type -> **Bearer Token**

# 4 - Acessando diretorio do Frontend

- Intale as dependencia do react **[ npm install ]**
- Apos isso **[ npm start ou yarn start ]**
- Aplicação ira na porta 3000