<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::firstOrCreate([
            'name' =>'vs datta imagem',
            'email' => 'dev@sv.com.br',
            'password' => Hash::make('sv@123')
        ]);
    }
}
