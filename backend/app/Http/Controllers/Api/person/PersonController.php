<?php

namespace App\Http\Controllers\Api\person;

use App\Http\Controllers\Controller;
use App\Models\person\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->getPersonAll());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $this->ruleCreate($request);
        if(!empty($validation)) {
            return $validation;
        }

        $person = new Person();
        $person->nome = $request->input('nome');
        $person->email = $request->input('email');
        $person->sexo = $request->input('sexo');
        $person->data_nascimento = $request->input('data_nascimento');
        $person->naturalidade = $request->input('naturalidade');
        $person->nacionalidade = $request->input('nacionalidade');
        $person->cpf = $request->input('cpf');
        $person->save();

        return response()->json(['success'=>'Adicionado com Sucesso']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        if(!is_numeric($id)){
            return response()->json(['error'=>'ID invalido'],404);
        }

        $person = $this->getPersonByID($id);

        return response()->json($person);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!is_numeric($id)){
            return response()->json(['error'=>'ID invalido'],404);
        }

        $validation = $this->ruleUpdate($request,$id);
        if(!empty($validation)) {
            return $validation;
        }

        $person = $this->getPersonByID($id);

        if(!$person->isEmpty())  {
            $person = Person::where('id',$id);
            $person->update([
                'nome' => $request->input('nome'),
                'email' => $request->input('email'),
                'sexo' => $request->input('sexo'),
                'data_nascimento' => $request->input('data_nascimento'),
                'naturalidade' => $request->input('naturalidade'),
                'nacionalidade' => $request->input('nacionalidade'),
                'cpf' => $request->input('cpf'),
            ]);

            return response()->json(['success'=>'Atualizado com Sucesso']);
        }

        return response()->json(['error'=>'Objeto não localizado'],404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!is_numeric($id)){
            return response()->json(['error'=>'ID invalido'],404);
        }

        $person = $this->getPersonByID($id);

        if(!$person->isEmpty()) {
            Person::destroy($id);
            return response()->json(['success'=>'Excluido com Sucesso'],200);
        }else {
            return response()->json(['error'=>'Objeto não localizado'],404);
        }

    }

    private function getPersonByID($id) {
        return person::where('id', $id)->get();
    }

    private function getPersonAll() {
        return Person::all();
    }

    private function ruleCreate(Request $request)
    {

        $rules = [
            'nome'            => 'required|min:10|max:160|string|unique:persons,nome',
            'email'           => 'nullable|email',
            'sexo'            => 'nullable|in:masculino,feminino',
            'data_nascimento' => 'required|min:10|max:160',
            'naturalidade'    => 'nullable|max:50',
            'nacionalidade'   => 'nullable|max:50',
            'cpf'             => ['required','unique:persons,cpf','regex:/^([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})$/'],
        ];

        $messages = [
            'required' => 'O campo :attribute é obrigatório',
            'min' => 'O campo :attribute necessário no mínimo :min caracteres',
            'max' => 'O campo :attribute necessário no máximo :max caracteres',
            'cpf.regex' => 'O cpf é incorreto ou invalido',
        ];

        $validator = Validator::make($request->input(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
    }

    private function ruleUpdate(Request $request, $id){

        $rules = [
            'nome' => [
                'required','min:10','max:50','string',
                Rule::unique('persons','nome')->ignore($id),
            ],
            'cpf' => [
                'required','string','regex:/^([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})$/',
                Rule::unique('persons','cpf')->ignore($id),
            ],
            'email'           => 'nullable|email',
            'sexo'            => 'nullable|in:masculino,feminino',
            'data_nascimento' => 'required|min:10|max:160',
            'naturalidade'    => 'nullable|max:50',
            'nacionalidade'   => 'nullable|max:50',
        ];

        $messages = [
            'required' => 'O campo :attribute é obrigatório',
            'min' => 'O campo :attribute necessário no mínimo :min caracteres',
            'max' => 'O campo :attribute necessário no máximo :max caracteres',
            'cpf.regex' => 'O cpf é incorreto ou invalido',
        ];

        $validator = Validator::make($request->input(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
    }
}
